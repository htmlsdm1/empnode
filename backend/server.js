const express = require('express')
const cors=require('cors')
const empRouter=require('./routes/emp')

const app = express();
app.use(cors('*'))
app.use(express.json())
//add the router
app.use('/emp', empRouter)

app.listen(5000, '0.0.0.0', () => {
    console.log('server started successfully on port 4000')
})