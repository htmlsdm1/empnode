const { request, response } = require('express')
const db = require('../db')
const utils = require('../utils')
const express = require('express')
const router = express.Router()

router.post('/addEmp', (request, response) => {
    const { emp_name, emp_Lastname, emp_DOB, city_Name } = request.body
    
    const query = 'insert into emp ( emp_name, emp_Lastname, emp_DOB, city_Name ) values(?,?,?,?)'
    db.pool.execute(query, [emp_name, emp_Lastname, emp_DOB, city_Name], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/getEmp', (request, response) => {
    const query = 'select emp_id, emp_name, emp_Lastname, emp_DOB, city_Name from emp'
    db.pool.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('/updateEmp/:emp_id', (request, response) => {
    const { emp_id } = request.params;
    const { emp_name, emp_Lastname, emp_DOB, city_Name} = request.body
    
    const query = 'update emp set emp_name = ?, emp_Lastname = ?, emp_DOB = ?, city_Name = ? where emp_id = ?';

    db.pool.execute(query, [emp_name, emp_Lastname, emp_DOB, city_Name, emp_id], (error, result) => {
        response.send(utils.createResult(error, result));
    })
})

router.delete("/deleteEmp/:emp_id", (request, response) => {
    const { emp_id } = request.params;
    const query =
      "delete from emp where emp_id = ?";
  
    db.pool.execute(query, [emp_id], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  });

module.exports = router
